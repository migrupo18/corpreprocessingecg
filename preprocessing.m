% Electrocardiogram (ECG) signal pre-processing usually consist of several steps, being some of the most
% common the following:
% • Recording spikes (artifacts of great amplitude) removal.
% • Low-pass filtering.
% • Powerline interference filtering.
% • Baseline wander and offset removal.
% Using Matlab, prepare a code for ECG signal pre-processing that contains all the previous steps. The code
% must also detect if a given channel is not connected. You can test your code using the signals provided in
% the ecgConditioningExample.mat file.
%%
function ecg_f = preprocessing(ecg,fs,flagfig)
% Inputs 
%   ecg: matrix of ecg-channel arrays
%   fs: frequency sample
%   flagfig: 1 = Display plots/figures
% Outputs
%   ecg_f: return filtered and preprocessed ecg-channels matrix
%%%%%
% Function:
% - Iterate over ECG channels (column-wise). Check if the channel is
% disconnected. Send a warning.
% - Use a median + std treshold and linear interpolation to
% correct spikes. 
% - Use bandpass filtering to remove baseline, offset and noises/artifacts.
%%%%%

% For loop is column-wise, check shape of ecg matrix
nrows = size(ecg,1); ncols = size(ecg,2);
if nrows < ncols  % if ecg channels are per rows
    ecg = ecg';  % change to column-wise shape;
   % if channles are per columns
    % nothing
end
preallocate = zeros([nrows,ncols]);  % preallocate matrix to keep preprocessed channels
disconnected = cell(1,ncols); % save idx disconnected channels
for m = 1:ncols
    sig = ecg(:,m); % Select channel
    
% ¿Disconnected channel?
    % calculate squared mean to give more weight to values further from 0.
    % If squared mean is 0, then channels is disconnected.
    if mean(sig.^2) == 0 
        ts = sprintf(['Channel number ' num2str(m) ' is disconnected\nColumn ' num2str(m) ' will be filled with zeros']);
        warning(ts)
        disconnected{m} = 'Y';
        continue    % Next iteration
        % Channel stays as zeros array
    end
   
% Start preprocessing
% 1. Recording spikes removal: std filter and interpolation correction
 
    sig_sp = sig;  % rellocating signal
    % spikes are really high amplitud, those greater than a trheshold 3 times 
    % median + std of signal are labelled as spikes (NaN)
    sig_sp([sig > 3*(median(sig)+std(sig))]) = NaN; 

    if any(isnan(sig_sp)) % Check if spike correction is needed
        px = 1:numel(sig); % points of signal
        sig_sp(isnan(sig_sp)) = interp1(px(~isnan(sig_sp)),sig_sp(~isnan(sig_sp)),px(isnan(sig_sp)),'linear','extrap');
    % Then, linear interpolation of NaN (outliers labelled values): px(signal==NaN)
    end
    
    % Example of threshold applied to spikes
    % figure,plot(signal),hold on, plot(repmat(3*(median(signal)+std(signal)),1,length(signal))),hold off
    
% 2. Baseline wander and offset removal + denoising + powerline interference
%    removal: 
    % Baseline wander frequency is usually considered < 0.5 Hz.
    % Noises and artifacts in ECG are considered higher frequency than ECG.
    % We can combine denoising (low-pass filtering) with
    % baseline + offset removal (high-pass filtering) in a bandpass filt.
        % *There could have been other methods such us detrending, DWT,etc
        % for baseline removal
    % We can use a Chebyshev filter to implement at same time a bandstop
    % filter to removal powerline interference (50 Hz in European powerline net)
        
    % Chebyshev Type II filter
    fn=fs/2;  % Nyquist anti-aliasing theorem
    Wp = [2  40]/fn; % Pass-frequencies
    Ws = [1  50]/fn; % Cut-off frequencies
    Rp =  3;         % Passband Ripple (3db variation in bandpass)
    Rs = 20;         % Stopband attenuation (-20 dB attenuation)
    [n,Ws] = cheb2ord(Wp,Ws,Rp,Rs);  % Select order of filter and 
    % Chebyshev natural frequency (Ws)
    [z,p,k] = cheby2(n,Rs,Ws);  % Chebyshev Filter Transfer Function Coeff
    [SOS,G] = zp2sos(z,p,k);   % Second-Order convertion (give + stability)
    sig_f = filtfilt(SOS,G,sig_sp);  % Forward & backward reverse IIR Filtering
    if flagfig == 1  % If flag for plot images was selected
        % Plot Frequency response of digital filter
    end 
    % Smooth data with a factor 0.1 for final preprocessed ECG
    y = smoothdata(sig_f,'gaussian','SmoothingFactor',0.1);
    preallocate(:,m) = y;
end
ecg_f = preallocate;
if flagfig == 1
    % Example of Response of used Chebyshev bandpass filter
    figure, freqz(SOS, 2^16, fs)
        title('Freq. Response of bandpass Chebyshev Filter')
 
    % Ploting Preprocessed ECG channels
    t = [1:nrows]/fs;
    for n = 1:6
    figure
    plot(t,ecg(:,n)),hold on
    plot(t,ecg_f(:,n)),hold off
    legend('Original ECG', 'Preprocessed ECG')
    if disconnected{n}=='Y'
        title(['Preprocessing of Channel ' num2str(n) ' (DISCONNECTED)'])
    else
    title(['Preprocessing of Channel ' num2str(n)])
    end
    ylim([-40000 80000])
    end
end
end